﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf210421b
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Person P { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            P = new Person() { Name = "Danny Cohen" , Id = 1};

            this.DataContext = this;
        }

        public class Person
        {
            public string Name { get; set; }

            public int Id { get; set; }

            public override string ToString()
            {
                return $"Person {Name} {Id}";
            }
        }
    }
}
